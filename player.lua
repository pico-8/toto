function init_player()
    p={
        sp=1,
        x=0,
        y=0,
        w=8,
        h=8,
        box={x1=0,y1=0,x2=7,y2=7},
        flp=false,
        dx=0,
        dy=0,
		max_dx_start=2,
        max_dx=2,
        max_dy=3,
        acc=0.5,
        jump_boost=1,
        jump_delta=.5,
        jump_count=0,
        jump_max=1.1,
        jump=false,
        anim=0,
		anim_run_delay=.1,
		wafing_timer=0,
        running=false,
        jumping=false,
        falling=false,
        sliding=false,
        landed=false,
		contactponey=false,
		wafing=false,
		wafing_ended=true,
		speed=false,
        croq=0,
		speedt=0,
		max_speedt=100
    }
end

function player_update()

	if not p.wafing then
		if btn(⬅️) then inptl=true else inptl=false end
		if btn(➡️) then inptr=true else inptr=false end
		if btn(⬆️) then inptu=true else inptu=false end
	elseif p.wafing then
		p.dx=0
		inptl=false
		inptr=false
		inptu=false
	end

	--physics
	p.dy+=gravity
	p.dx*=friction
	
	--controls
	if inptl then
		p.dx-=p.acc
		p.running=true
		p.flp=true
	end
	if inptr then
		p.dx+=p.acc
		p.running=true
		p.flp=false
	end
	
	--slide
	if p.running
	and not inptl
	and not inptr
	and not p.falling
	and not p.jumping then
		p.running=false
		p.sliding=true
	end
	
	--jump
	if inptu then
		if p.landed
		and not p.jump then
			p.jump=true
			p.dy-=p.jump_boost
			p.landed=false
		else
			if p.jump_count<p.jump_max
			and p.jump then
				p.dy-=p.jump_boost
				p.jump_count+=p.jump_delta
			end
		end
	else
		p.jump=false
		p.jump_count=0
	end
 
--check collision up and down
if p.dy>0 then
 	p.falling=true
 	p.landed=false
 	p.jumping=false
 	
 	p.dy=limit_speed(p.dy,p.max_dy)
 	
 	if collide_map(p,"down",0) then
 		p.landed=true
 		p.falling=false
 		p.dy=0
 		p.y-=((p.y+p.h+1)%8)-1
		end
	elseif p.dy<0 then
		p.jumping=true
		if collide_map(p,"up",1) then
			p.dy=0
		end
	end
	
	--check collision left and right
	if p.dx<0 then
		p.dx=limit_speed(p.dx,p.max_dx)
		if collide_map(p,"left",1) then
			p.dx=0
		end
	elseif p.dx>0 then
		p.dx=limit_speed(p.dx,p.max_dx)
		if collide_map(p,"right",1) then
			p.dx=0
		end
	end
	
	--check croqs collision
	for q in all(croqs.instances) do
		if coll(p,q) then
			del(croqs.instances,q)
			p.croq+=1
		end
	end

	--check cats collision
	for c in all(cats.instances) do
		if coll(p,c) then
			del(cats.instances,c)
			p.speed=true
		end
	end

	--check poney collision
	for n in all(poneys.instances) do
		if coll(p,n) then
			p.contactponey=true
		else
			p.contactponey=false
		end
	end

	if p.contactponey then
		poney_loop()
	else
		p.wafing_timer=0
	end

	function poney_loop()
		local time_delay=150
		if p.wafing_timer<time_delay then
			p.wafing=true
			p.wafing_timer+=1
		elseif p.wafing_timer==time_delay then
			p.wafing=false
		end
	end

	--stop sliding
	if p.sliding then
		if p.wafing then sliding_limit=.8 else sliding_limit=.2 end
		if abs(p.dx)<sliding_limit
		or p.running then
			p.dx=0
			p.sliding=false
		end
	end

	--speed bonus
	if p.speed then
		if (p.speedt<p.max_speedt) then
			p.max_dx=5
			p.speedt+=1
			p.anim_run_delay=.05
		elseif (p.speedt>=p.max_speedt) then
			p.max_dx=p.max_dx_start
			p.speedt=0
			p.anim_run_delay=.1
			p.speed=false
		end
	end
	
	p.x+=p.dx
	p.y+=p.dy
	
	--limit player to map
	if p.x<map_start then
		p.x=map_start
	end
	if p.x>map_end-p.w then
		p.x=map_end-p.w
	end
	
	--eating croqs
	if collide_map(p,"croq",2) then
		printh("miam!")
	end

	--check loose by fall
	if p.y > (level+1)*128 then init_level(level) end
end

function limit_speed(num,maximum)
	return mid(-maximum,num,maximum)
end

function player_bonus_speed()
	local bonus_time=0
	if bonus_time<100 then
		p.max_dx=5
		bonus_time+=1
	elseif bonus_time>=100 then
		p.max_dx=2
		bonus_time=0
	end
end