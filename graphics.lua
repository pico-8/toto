function menu_draw()
    cls(11)
    print("appuie sur X pour lancer le jeu!",1,1,1)
end

function game_draw()
    cls(back_color)
    map(0,level)

    layers_draw({clouds})
    draw_object(croqs)
    draw_object(cats)
    draw_object(poneys)
    player_animate()
	player_draw()
    score_draw()
	speed_draw()
    rules_draw()
end

function layers_draw(layers)
	for l in all(layers) do
		for i=0,l.repet do
			for s in all(l.seq) do
				spr(s.sp,(i*l.w+s.x+cam_x)*l.coef,s.y,s.w/8,s.h/8,false,false)
			end
		end
	end
end

function score_draw()
    spr(16,cam_x+2,cam_y+2,1,1,false)
    print(p.croq,cam_x+11,4)
end

function speed_draw()
	if p.speedt>0 then
		//rectfill(cam_x+50,cam_y+2,(cam_x+10+15*(p.speedt/100)),(cam_y+2+15*(p.speedt/100)),0)
		//rectfill(cam_x+0,cam_y+0,(cam_x+0)+80*(p.speedt/100),cam_y+7,2)rectfill(cam_x+0,cam_y+0,(cam_x+0)+80*(p.speedt/100),cam_y+7,2)
		rectfill(
			cam_x+18,
			cam_y+6,
			cam_x+18+40*(1-(p.speedt/p.max_speedt)),
			cam_y+6,
			9
		)
	end
end

function draw_object(obj)
	local i
	for i in all(obj.instances) do
		spr(obj.sp,i.x,i.y,obj.spx,obj.spy,false,false)
	end
end

function init_waf_box()
	waf_box={x=p.x-3,y=p.y-3}
end

function write_waf(box)
	print("waf!",waf_box.x,waf_box.y)
	waf_box.y-=1
end

function player_animate()
	if p.jumping then
		p.sp=7
	elseif p.falling then
		p.sp=8
	elseif p.sliding then
		p.sp=9
	elseif p.wafing then
		if p.sp!=18 and p.sp!=19 then p.sp=18 end
		if p.sp==18 then init_waf_box() end
		if p.sp==19 then write_waf() end
		if time()-p.anim>.3 then
			p.anim=time()
			p.sp+=1
			if p.sp>19 then
				p.sp=18
			end
		end
	--elseif not p.wafing then wafing_start=false
	elseif p.running then
		if time()-p.anim>p.anim_run_delay then
			p.anim=time()
			p.sp+=1
			if p.sp>6 then
				p.sp=3
			end
		end

	else --player idle
		if time()-p.anim>.3 then
			p.anim=time()
			p.sp+=1
			if p.sp>2 then
				p.sp=1
			end
		end
	end
end

function player_draw()
	spr(p.sp,p.x,p.y,1,1,p.flp)
end

function rules_draw()
    print("ramasse toutes les croquettes!",2,95,7)
	print("course un chat",176,88,7)
	print("pour gagner un",176,96,7)
	print("bonus de vitesse!",176,104,7)
end