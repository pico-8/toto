function debug(deb)
    local ctn=0
    for d in all(deb) do
        print(d[1]..": "..d[2],cam_x+2,cam_y+120-ctn*6,0)
        ctn+=1
    end
end

function init_level(l)
    scene="game"
    level=l
    init_object_instances_by_level(croqs)
    init_object_instances_by_level(cats)
    init_object_instances_by_level(poneys)
    set_player_position(l)
    --gestion caméra / init toto / croquettes / chats / etc.
end

function init_object_instances_by_level(obj)
    local o
    for o in all(obj.coords["l"..level]) do
        add(obj.instances,  {
            x=o[1],
            y=o[2],
            box={x1=0,y1=0,x2=obj.w-1,y2=obj.h-1},
            sp=obj.sp
        })
    end
end

function get_max_objets_instances(obj)
    return #obj.coords["l"..level]
end

function camera_update()
    cam_x=p.x-64+(p.w/2)
    cam_y=level*128
    if cam_x<map_start then
        cam_x=map_start
    end
    if cam_x>map_end-128 then
        cam_x=map_end-128
    end
    camera(cam_x,cam_y)
end

function check_level_end()
    if p.croq==get_max_objets_instances(croqs) then
        scene="changelevel"
    end
end

function set_player_position(l)
    p.x=pip["p"..l][1]
    p.y=pip["p"..l][2]
end