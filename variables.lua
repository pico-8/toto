function init_variables()
    gravity=0.3
    friction=0.85
	
    scene="menu"
	back_color=12
	player_drive=true

    --parallax layers
    clouds={
		coef=.5,
		w=200,
		repet=5,
		seq={
			{sp=128,w=16,h=16,x=20,y=10},
			{sp=128,w=16,h=16,x=100,y=15}
		}
	}

    mountains={
		coef=.2,
		w=900,
		repet=5,
		seq={
			{sp=130,w=64,h=64,x=14,y=56}
		}
	}

	--croqs
	croqs={
		instances={},
		w=8,
		h=8,
		sp=16,
		spx=1,
		spy=1,
		coords={
			l0={
				{85,80},
				{5,20}
			}
		}
	}

	--cats
	cats={
		instances={},
		w=8,
		h=8,
		sp=17,
		spx=1,
		spy=1,
		coords={
			l0={
				{264,72}
			}
		}
	}

	--poneys
	poneys={
		instances={},
		w=22,
		h=18,
		sp=10,
		spx=3,
		spy=3,
		coords={
			l0={
				{512,70}
			}
		}
	}

	--players init positions by level
	pip={
		p0={8,80}
	}

    --simple camera
	cam_x=0
	cam_y=0
	
	--map limits
	map_start=0
	map_end=1024

	--set transparency colors
	palt(0,false)
    palt(12,true)
end

function wait(a) for i = 1,a do flip() end end